const express = require("express");

// Called at each request
function allInit() {
  console.log("[allInit] - Request received");
}

// Called at each request to /auth/*
function allAuth() {
  console.log("[allAuth] - Request auth received");
}

// Called at / and /index with GET
function getIndex(rep, res) {
  console.log("[getIndex] - Request root received");
  getFile(res, "/index.html");
}

// Called at /form with GET
function getForm(rep, res) {
  console.log("[getForm] - Request form received");
  getFile(res, "/form.html");
}

// Called at /form with POST
function postForm(req, res) {
  console.log("[postForm] - Request form received");
  res.json('{ result: "Success" }');
}

function getFile(res, filename) {
  res.sendFile(__dirname + filename, function(err) {
    if (err) {
      res.sendStatus(404);
      next(err);
    }
  });
}

exports.allInit = allInit;
exports.allAuth = allAuth;
exports.getIndex = getIndex;
exports.getIndex = getIndex;
exports.getForm = getForm;
exports.postForm = postForm;
