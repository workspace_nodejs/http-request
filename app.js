const bodyParser = require("body-parser");
const express = require("express");
const app = express();
const port = 1337;
const router = express.Router();

const controller = require("./controller");

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.listen(port, function() {
  console.log("Le serveur fonctionne sur le port " + port);
});

router.route("*").all(function(req, res, next) {
  controller.allInit();
  next();
});

router.route("/auth/*").all(function(req, res, next) {
  controller.allAuth(req, res);
});

router.get(["/", "/index"], function(req, res) {
  controller.getIndex(req, res);
});

router
  .route("/form")
  .get(function(req, res) {
    controller.getForm(req, res);
  })
  .post(function(req, res) {
    controller.postForm(req, res);
  });

app.use(router);
